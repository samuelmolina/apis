const express = require('express')
const app = express()
const port = 3000;

const knex = require('knex')({
  client: 'mysql',
  connection: {
    host : '127.0.0.1',
    user : 'root',
    password : '',
    database : 'usuarios'
  }
});

 //home / lista Usuarios
app.get('/', (req, res) => {
	const campos = ['nombre'];
	const query = knex('usuario').select(campos);
	query.then(data => res.json(data))
})

//agregar  Usuario
app.get('/addUser', (req, res) => {
	const {nombres,apellidos, dni} = req.query;
	const data = {nombres, apellidos, dni}; 
	const query = knex('usuario').insert(data);
	query.then(idUsuario => {
		//const nuevo = knex('usuario').where('id_usuario',idUsuario);
		//nuevo.then(data => {
		//	res.json(data);
		//})

		data.id_usuario = idUsuario;
		res.json(data);
	})  
})

//obtener usuario por id
app.get('/getUser', (req, res) => {
	const {id} = req.query;
	const usuario = knex('usuario').where('id_usuario', id); 
	usuario.then( data => res.json(data) )
})

//Editar usuario (Actualizar)
app.get('/updateUser', (req, res) => {
	const {id, nombres, apellidos, dni} = req.query;
	const editar = knex('usuario').where('id_usuario',id)
	.update({
		nombres,
		apellidos,
		dni 
	})
	editar.then(()=>{
		const resultado = knex('usuario').where('id_usuario',id);
		resultado.then(data=> res.json(data))
	})
})

//Eliminar usuario
app.get('/deleteUser', (req, res) => {
	let {id} = req.query;
	const eliminar = knex('usuario').where('id_usuario',id).del()
	eliminar.then(id => res.json(id))
})

app.listen(port,()=>{
	console.log(`Servidor Activo: puerto ${port}`);
})
